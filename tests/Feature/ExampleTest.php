<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeatureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    
    public function test_feature()
    {
        $response = $this->get('/cart-test');

        $response->assertStatus(200);

        
    }


    public function test_calculation()
    {
        $data = [
            'name'           => ['product_1','product_2'],
            'price'          => ['100','200'],
            'quantity'       => ['3','2'],
            'shipping_fees'  => 100,
        ];
        
        $response = $this->json('POST', '/cart-items', $data)
            ->assertStatus(200)
            ->assertJsonStructure([
                "data",
                "message"
            ]);

    }

    

    


}
