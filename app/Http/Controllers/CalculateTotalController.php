<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalculateTotalController extends Controller{


    public function calculateTotals(Request $request){

        $name           = ['product_1','product_2'];
        $price          = ['100','200'];
        $quantity       = ['3','2'];
        $shipping_fees  = 100;

        $sub_total          = 0;
        $products_price     = [];
        $shipping_tax_fees  = 0;

        $tax = (object)array();
        $tax->rate = 10.1212;
        $tax->shipping_incl = true;


        try {

            if(count($name) == count($price) && count($price) == count($quantity) ){
                
                foreach($price as $index => $single_price){
                    $product_price[] = trim($single_price) * trim($quantity[$index]);
                }

                $products_price = array_sum($product_price);

                if($tax->shipping_incl==true && $shipping_fees > 0){
                    
                    $shipping_tax_fees = $shipping_fees + ( ($shipping_fees * round($tax->rate, 2) ) / 100 );

                }

                $sub_total = $products_price + $shipping_tax_fees;
                
            }

        } catch (Throwable $e) {
            // report($e);
            return false;
        }

    }



    public function _calculateTotals(Request $request){
        
        $validated = $request->validate([
            'name.*' => 'required',
            'price.*' => 'required',
            'quantity.*' => 'required',
            'shipping_fees' => 'required',
        ]);


        $name           = $request->name;
        $price          = $request->price;
        $quantity       = $request->quantity;
        $shipping_fees  = $request->shipping_fees;
        $tax            = $request->tax;
        
        $sub_total          = 0;
        $products_price     = [];
        $shipping_tax_fees  = 0;
        
        $tax = (object)array();
        $tax->rate = 10.1212;
        $tax->shipping_incl = true;
        
        
        try {
        
            if(count($name) == count($price) && count($price) == count($quantity) ){
                
                foreach($price as $index => $single_price){
                    $product_price[] = trim($single_price) * trim($quantity[$index]);
                }
        
                $products_price = array_sum($product_price);
        
                if($tax->shipping_incl==true && $shipping_fees > 0){
                    
                    $shipping_tax_fees = $shipping_fees + ( ($shipping_fees * round($tax->rate, 2) ) / 100 );
        
                }
        
                $sub_total = $products_price + $shipping_tax_fees;
        
                return response( array('data'=> $sub_total,'message'=>'Successfully Calculated'),200);
                
            }
        
        } catch (Throwable $e) {
            
            return response ( array('data'=>'','message'=> "Failed due to ".$e),466);

        }
        
    }


    
}
